package actions

import "github.com/gobuffalo/buffalo"

// UserHandler is a default handler to serve up
// user information.
func UserHandler(c buffalo.Context) error {
	return c.Render(200, r.JSON(map[string]string{"message": "User Endpoint"}))
}
